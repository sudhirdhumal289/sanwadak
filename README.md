# Sanwadak - Library which helps to communicate with multiple systems with simple configuration.

This library provides you an ability to comminucate with multiple systems from UI.
This library acts as a gateway on client side, it does measure the performance of each server
and re-route the request to better performing server.

# Testing

TODO

# How To

Please stay tuned to see how you can configure the library and use it in your code base

Initial configuration snippet

```typescript
import {
  IApiConfig,
  IStrategyConfig,
  IRetryPolicy,
  IClusterConfig,
  HttpVerb,
} from "sanwadak";
import Clusters from "./clusters";
import currentEnv from "../../config/currentEnv";

import clusterDomainMapping from "./clusterDomainMapping";
import Env from "../../common/Env";

const retryPolicy: IRetryPolicy = {
  delayTimeInMilliSeconds: 1000,
  retryCount: 3, // Retry attempts are only for 500+ HTTP status code
};

const strategyConfig: IStrategyConfig = {
  timeout: 1000 * 60,
  retryPolicy: retryPolicy,
};

const clusters: IClusterConfig[] = [];

const pathPrefix = currentEnv === Env.LOCAL ? "/dev" : "";
const idmCluster: IClusterConfig = {
  id: Clusters.IDM,
  displayName: "IDM Cluster",
  baseUrls: clusterDomainMapping[currentEnv][Clusters.IDM] || [],
  minServerCountForPerfCalc: 5,
  defaultHeaders: {
    "content-type": "application/json",
    "accept-api-version": "resource=1.0, protocol=1.0",
    accept: "*/*",
  },
  endpointConfigs: [
    {
      id: "authenticate",
      path:
        `${pathPrefix}/idm/am/json/internal/authenticate?` +
        "authIndexType={authIndexType}&authIndexValue={authIndexValue}",
      verb: HttpVerb.POST,
    },
    {
      id: "getSSOToken",
      path:
        `${pathPrefix}/idm/am/json/realms/internal/authenticate?` +
        "state={state}&code={code}&scope={scope}&authuser={authuser}&hd={hd}&prompt={prompt}",
      verb: HttpVerb.POST,
    },
  ],
};

clusters.push(idmCluster);

const apiConfig: IApiConfig = {
  clusters: clusters,
  strategyConfig: strategyConfig,
  debugMode: false,
};

export default apiConfig;
```

# Contribution

# License

This project is dual licensed under Apache 2.0 or MIT.

# TODO

- Endpoint can specify the dependency on other endpoint in same or different cluster
- Prevent repetative API call within fraction of seconds
- Provision to register an interceptor
- Provision to inject response validator
- response mapper - Receive response in any format and parse it and then map with expected type
- API Chaining
  - Call multiple endpionts in chain
  - Execute or invoke endpoints in parallel or serial fashion
  - Ability of having chain inside a chain

import { IApiConfig } from '../types/api.types';
import Cluster from './Cluster';
import Server from './Server';

class Api {
  private static clusters: Map<string, Cluster> = new Map();
  protected serverInstance: Server;

  constructor(clusterId: string) {
    this.serverInstance = this.getServerInstance(clusterId);
  }

  private getServerInstance(clusterId: string): Server {
    const cluster = Api.clusters.get(clusterId);

    if (!cluster) throw new Error('Server cluster is not configured.');

    const serverInstance = cluster.getServerInstance();

    if (!serverInstance) {
      const errorMsg = `Failed to get the server instance from cluster 
                        ${cluster.getDisplayName() || cluster.getClusterId()}`;
      throw new Error(errorMsg);
    }

    return serverInstance;
  }
  
  static configure(config: IApiConfig) {
    if (Api.clusters.size > 0) return;

    if (!config.clusters || config.clusters.length == 0) {
      throw new Error('At-least one cluster instance needs to be configured.');
    }

    for (const clusterConfig of config.clusters) {
      if (clusterConfig.id.trim().length === 0) {
        throw new Error('Cluster ID can not be empty');
      }

      const cluster = Api.clusters.get(clusterConfig.id) || new Cluster(clusterConfig);
      
      Api.clusters.set(cluster.getClusterId(), cluster);
    }
  }
}

export default Api;

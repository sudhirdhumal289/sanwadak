import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig, Method } from 'axios';
import { HttpVerb, IServerConfig } from '../types/api.types';
import { IRequestConfig, INoDataRequestConfig, IRequestLog } from '../types/request.types';
import Endpoint from './Endpoint';
import { defaultStrategyConfig } from './defaults';
import retry from './retry';
import PerfAnalysis from './PerfAnalysis';
import RequestError from './RequestError';

class Server {
  private config: IServerConfig;
  private axiosInstance: AxiosInstance;
  private endpoints: Map<string, Endpoint> = new Map();
  private _debugMode = false;
  private readonly availableActions: Map<string, string | undefined> = new Map();
  private perfAnalysis: PerfAnalysis = new PerfAnalysis();

  constructor(config: IServerConfig) {
    this.config = config;

    // TODO - Validate the configuration.

    this.axiosInstance = this.getAxiosInstance();
    this.configure();
  }

  private configure(): void {
    // Prepare all the instances of endpoints.
    this.config.endpointConfigs.forEach((endpointConfig) => {
      try {
        const endpoint = new Endpoint(endpointConfig);

        if (endpoint) {
          this.endpoints.set(endpointConfig.id, endpoint);
          this.availableActions.set(endpointConfig.id, endpointConfig.displayName);
        }
      } catch (error) {
        // Here we just need to log an error and skip this invalid endpiont.
        console.log(error);
      }
    });
  }

  private getAxiosInstance(): AxiosInstance {
    return axios.create({
      baseURL: this.getBaseUrl(),
      timeout: this.config.strategyConfig?.timeout,
    });
  }

  private printApiResponse(axiosResponse: AxiosResponse) {
    if (!this._debugMode) return;

    console.debug('Data:');
    console.dir(axiosResponse.data);
    console.table({ status: axiosResponse.status, statusText: axiosResponse.statusText });
    console.table(axiosResponse.headers);
  }

  private printRequestDetails<D>(requestconfig: IRequestConfig<D>, axiosRequestConfig: AxiosRequestConfig) {
    if (!this._debugMode) return;

    console.debug(`${axiosRequestConfig.method as Method}: ${axiosRequestConfig.baseURL as string}`);

    if (axiosRequestConfig.headers) console.table(axiosRequestConfig.headers);

    if (axiosRequestConfig.params) console.table(axiosRequestConfig.params);

    if (requestconfig.pathParams) console.table(requestconfig.pathParams);
  }

  private logApiError(error: unknown) {
    if (axios.isAxiosError(error)) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.error(error.response.data);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.error(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.error('Error', error.message);
      }
    }

    console.error('Something went wrong while calling an API', error);
  }

  private isRequestLog(log: Omit<IRequestLog, 'endTime'> | IRequestLog[]): log is IRequestLog {
    return (log as Omit<IRequestLog, 'endTime'>).startTime !== undefined;
  }

  private isRequestLogArray(log: Omit<IRequestLog, 'endTime'> | IRequestLog[]): log is IRequestLog[] {
    return (log as IRequestLog[]).length !== undefined;
  }

  private addRequestLogs(requestLogs: Omit<IRequestLog, 'endTime'> | IRequestLog[]): void {
    if (this.isRequestLog(requestLogs)) {
      this.perfAnalysis.addRequestLog({
        ...requestLogs,
        endTime: new Date(),
      });
    } else if (this.isRequestLogArray(requestLogs)) {
      this.perfAnalysis.addRequestLogs(requestLogs);
    }
  }

  private getAxiosRequestConfig<D>(
    endpoint: Endpoint,
    requestConfig: IRequestConfig<D>,
    method: HttpVerb,
  ): AxiosRequestConfig<D> {
    const actionUrl = endpoint.getParsedPath(requestConfig.pathParams);

    const axiosRequestConfig: AxiosRequestConfig<D> = requestConfig.axiosRequestConfig || {};
    axiosRequestConfig.baseURL = this.config.baseUrl;
    axiosRequestConfig.params = requestConfig.searchParams;
    axiosRequestConfig.url = actionUrl;
    axiosRequestConfig.method = method;

    // If default headers are setup then pass them
    axiosRequestConfig.headers = { ...this.config.defaultHeaders, ...requestConfig.headers };

    const noReqBodyHeaders = [HttpVerb.GET, HttpVerb.HEAD, HttpVerb.OPTIONS];
    if (noReqBodyHeaders.includes(method)) {
      delete axiosRequestConfig.data;
    } else if (requestConfig.payload) {
      // If request payload is available and HTTP method supports it then pass it on.
      axiosRequestConfig.data = requestConfig.payload;
    }

    return axiosRequestConfig;
  }

  private async performRetryLogic<T, D>(
    requestConfig: IRequestConfig<D>,
    axiosRequestConfig: AxiosRequestConfig<D>,
    error: unknown,
  ): Promise<AxiosResponse<T, D>> {
    if (axios.isAxiosError(error) && error.response && error.response.status >= 500) {
      const strategyConfig = this.config.strategyConfig || defaultStrategyConfig;

      if (requestConfig.retryEnabled && strategyConfig.retryPolicy && strategyConfig.retryPolicy.retryCount > 0) {
        const retryConfig = {
          args: [axiosRequestConfig],
          functionToExecute: this.axiosInstance.request.bind(this.axiosInstance),
          retryPolicy: strategyConfig.retryPolicy,
        };

        const { axiosResponse, requestLogs } = await retry(retryConfig);
        this.addRequestLogs(requestLogs);

        const axiosResp = axiosResponse as AxiosResponse<T, D>;

        this.printApiResponse(axiosResp);

        return axiosResp;
      }
    }

    if (axios.isAxiosError(error) && error.response) {
      throw new RequestError(error.message, error.response.data)
    }

    throw error;
  }

  private async request<T, D>(requestConfig: IRequestConfig<D>, method: HttpVerb): Promise<AxiosResponse<T, D>> {
    if (window && !window.navigator.onLine) {
      throw new RequestError('Unable to connect to internet');
    }

    const endpoint = this.endpoints.get(requestConfig.endpointId);

    if (!endpoint) {
      throw new RequestError(`Endpoint ${requestConfig.endpointId} does not exists or isn't configured.`);
    } else if (endpoint.getEndpointConfig().verb !== method) {
      throw new RequestError(`Endpoint ${requestConfig.endpointId} does not support ${method} method.`);
    }

    if (this.debugMode) {
      console.debug(`Invoking Endpoint: [${endpoint.getEndpointConfig().id}]
                      [${endpoint.getEndpointConfig().displayName || ''}]`);
    }

    const axiosRequestConfig: AxiosRequestConfig<D> = this.getAxiosRequestConfig(endpoint, requestConfig, method);

    this.printRequestDetails<D>(requestConfig, axiosRequestConfig);

    const startTime = new Date();

    try {
      const axiosResponse = await this.axiosInstance.request<T, AxiosResponse<T, D>, D>(axiosRequestConfig);

      this.addRequestLogs({ startTime, isFailed: false });

      this.printApiResponse(axiosResponse);

      return axiosResponse;
    } catch (error) {
      this.addRequestLogs({ startTime, isFailed: true });

      this.logApiError(error);

      return this.performRetryLogic<T, D>(requestConfig, axiosRequestConfig, error);
    }
  }

  set debugMode(debugMode: boolean) {
    this._debugMode = debugMode;
  }

  get debugMode(): boolean {
    return this._debugMode;
  }

  getId(): string {
    return this.config.id;
  }

  getClusterId(): string {
    return this.config.clusterId;
  }

  getAvailableActions(): string[] {
    return Array.from(this.availableActions.keys());
  }

  getBaseUrl(): string {
    return this.config.baseUrl;
  }

  getPerfAnalysis(): PerfAnalysis {
    return this.perfAnalysis;
  }

  async get<T>(requestConfig: INoDataRequestConfig): Promise<AxiosResponse<T, null>> {
    return this.request<T, null>(requestConfig, HttpVerb.GET);
  }

  async head(requestConfig: INoDataRequestConfig): Promise<AxiosResponse<null, null>> {
    return this.request<null, null>(requestConfig, HttpVerb.HEAD);
  }

  async post<T, D>(requestConfig: IRequestConfig<D>): Promise<AxiosResponse<T, D>> {
    return this.request<T, D>(requestConfig, HttpVerb.POST);
  }

  async put<D>(requestConfig: IRequestConfig<D>): Promise<AxiosResponse<null, D>> {
    return this.request<null, D>(requestConfig, HttpVerb.PUT);
  }

  async delete<T, D>(requestConfig: IRequestConfig<D>): Promise<AxiosResponse<T, D>> {
    return this.request<T, D>(requestConfig, HttpVerb.DELETE);
  }

  async options<T>(requestConfig: INoDataRequestConfig): Promise<AxiosResponse<T, null>> {
    return this.request<T, null>(requestConfig, HttpVerb.OPTIONS);
  }

  async patch<T, D>(requestConfig: IRequestConfig<D>): Promise<AxiosResponse<T, D>> {
    return this.request<T, D>(requestConfig, HttpVerb.PATCH);
  }
}

export default Server;

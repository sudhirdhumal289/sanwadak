import { nanoid } from 'nanoid';
import { IClusterConfig, IEndpointConfig, IServerConfig } from '../types/api.types';
import Server from './Server';

class Cluster {
  private id: string;
  private minServerCountForPerfCalc?: number;
  private servers: Array<Server> = [];
  private _debugMode = false;
  private _displayName?: string;

  constructor(clusterConfig: IClusterConfig) {
    this.id = clusterConfig.id;
    this.minServerCountForPerfCalc = clusterConfig.minServerCountForPerfCalc;
    this._displayName = clusterConfig.displayName;

    this.registerServersInCluster(clusterConfig);
  }

  private registerServersInCluster(clusterConfig: IClusterConfig): void {
    if (clusterConfig.baseUrls.length === 0) {
      if (window) {
        clusterConfig.baseUrls.push('');
      } else {
        throw new Error('Cluster can not be created, base URL(s) not configured');
      }
    }

    for (const baseUrl of clusterConfig.baseUrls) {
      const endpointConfigs = JSON.parse(JSON.stringify(clusterConfig.endpointConfigs))as IEndpointConfig[];
      let headers = {};
      if (clusterConfig.defaultHeaders) headers = clusterConfig.defaultHeaders;

      const serverConfig: IServerConfig = {
        id: nanoid(),
        baseUrl: baseUrl,
        clusterId: this.getClusterId(),
        strategyConfig: clusterConfig.strategyConfig,
        defaultHeaders: headers,
        endpointConfigs,
      };

      const serverInstance = new Server(serverConfig);
      serverInstance.debugMode = this.debugMode;

      this.servers.push(serverInstance);
    }
  }

  private sortByReqPerMin(servers: Array<Server>) {
    // Sort servers list using requests-per min metric
    servers.sort((serverA, serverB) => {
      const serverAPerf = serverA.getPerfAnalysis();
      const serverBPerf = serverB.getPerfAnalysis();

      if (
        serverAPerf.getTotalRequestsCount() === 0 ||
        serverAPerf.getRequestsPerMin() > serverBPerf.getRequestsPerMin()
      ) {
        return -1;
      } else if (
        serverBPerf.getTotalRequestsCount() === 0 ||
        serverAPerf.getRequestsPerMin() < serverBPerf.getRequestsPerMin()
      ) {
        return 1;
      }

      return 0;
    });
  }

  private sortBySuccessRate(serversList: Server[]) {
    serversList.sort((serverA, serverB) => {
      const serverAPerf = serverA.getPerfAnalysis();
      const serverBPerf = serverB.getPerfAnalysis();

      if (
        serverAPerf.getTotalRequestsCount() === 0 ||
        (serverAPerf.getSuccessRate() > 0.5 && serverAPerf.getSuccessRate() > serverBPerf.getSuccessRate())
      ) {
        return -1;
      } else if (
        serverBPerf.getTotalRequestsCount() === 0 ||
        (serverBPerf.getSuccessRate() > 0.5 && serverAPerf.getSuccessRate() < serverBPerf.getSuccessRate())
      ) {
        return 1;
      }

      return 0;
    });
  }

  private sortByAvgResponseTime(serversList: Server[]) {
    serversList.sort((serverA, serverB) => {
      const serverAPerf = serverA.getPerfAnalysis();
      const serverBPerf = serverB.getPerfAnalysis();

      if (
        serverAPerf.getTotalRequestsCount() === 0 ||
        serverAPerf.getAvgResponseTime() < serverBPerf.getAvgResponseTime()
      ) {
        return -1;
      } else if (
        serverBPerf.getTotalRequestsCount() === 0 ||
        serverAPerf.getAvgResponseTime() > serverBPerf.getAvgResponseTime()
      ) {
        return 1;
      }

      return 0;
    });
  }

  private getPerformantServerInstance(): Server {
    this.sortByReqPerMin(this.servers);

    // Pick list of servers which are performing well
    const listOfSelectedServers = this.servers.slice(0, Math.floor(this.servers.length / 2));
    const ignoredServerList = this.servers.slice(Math.floor(this.servers.length / 2));

    this.sortByAvgResponseTime(listOfSelectedServers);
    this.sortByAvgResponseTime(ignoredServerList);

    // If average response time of ignored server is better than then selected server
    // and success rate is greater than 50% then ignored server will be used.
    if (
      listOfSelectedServers[0] &&
      listOfSelectedServers[0].getPerfAnalysis().getTotalRequestsCount() > 0 &&
      ignoredServerList[0] &&
      ignoredServerList[0].getPerfAnalysis().getAvgResponseTime() <
        listOfSelectedServers[0].getPerfAnalysis().getAvgResponseTime() &&
      ignoredServerList[0].getPerfAnalysis().getSuccessRate() > 0.5
    ) {
      return ignoredServerList[0];
    }

    this.sortBySuccessRate(listOfSelectedServers);

    // Fist element in selected server has better average response time and success rate than the ignored server.
    return (listOfSelectedServers[0] ? listOfSelectedServers[0] : this.servers[0]) as Server;
  }

  set debugMode(debugMode: boolean) {
    this._debugMode = debugMode;
  }

  get debugMode(): boolean {
    return this._debugMode;
  }

  getClusterId(): string {
    return this.id;
  }

  getDisplayName(): string | undefined {
    return this._displayName;
  }

  getServerInstance(): Server | undefined {
    if (this.servers.length >= 2) {
      if (!this.minServerCountForPerfCalc || this.servers.length >= this.minServerCountForPerfCalc) {
        // Check load on each server and return server instance accordingly.
        return this.getPerformantServerInstance();
      } else {
        // Choose randomly
        return this.servers[Math.floor(Math.random() * this.servers.length)];
      }
    } else if (this.servers.length == 1) {
      return this.servers[0];
    }

    return undefined;
  }
}

export default Cluster;

import { IRequestLog } from '../types/request.types';

class PerfAnalysis {
  private requestsPerMinute: number;
  private failuresPerMinute: number;

  private maxResponseTime: number;
  private failedRequestsCount: number;
  private totalResponseTime: number;
  private totalResponseTimeOfFailedReq: number;
  private logRecordsScanned: number;
  private requestLogs: IRequestLog[];

  constructor() {
    this.failedRequestsCount = 0;
    this.totalResponseTime = 0;
    this.totalResponseTimeOfFailedReq = 0;
    this.requestLogs = [];
    
    this.requestsPerMinute = 0;
    this.failuresPerMinute = 0;
    this.maxResponseTime = 0;
    this.logRecordsScanned = 0;
  }

  private calculatePerf(): void {
    for (let i = this.logRecordsScanned; i < this.requestLogs.length; i++) {
      const requestLog = this.requestLogs[i] as IRequestLog;

      const respTimeInMs = requestLog.endTime.getTime() - requestLog.startTime.getTime();

      if (this.maxResponseTime < respTimeInMs) {
        this.maxResponseTime = respTimeInMs;
      }

      this.totalResponseTime += respTimeInMs;

      if (requestLog.isFailed) {
        this.failedRequestsCount++;
        this.totalResponseTimeOfFailedReq += respTimeInMs;
      }
    }

    this.logRecordsScanned = this.requestLogs.length;

    const totalTimeInMin = this.totalResponseTime / (1000 * 60);
    this.requestsPerMinute = totalTimeInMin / this.logRecordsScanned / 60;

    const totalTimeInMinOfFailedReq = this.totalResponseTimeOfFailedReq / (1000 * 60);
    this.failuresPerMinute =
      this.failedRequestsCount > 0 ? totalTimeInMinOfFailedReq / this.failedRequestsCount / 60 : 0;
  }

  addRequestLog(requestLog: IRequestLog): void {
    this.requestLogs.push(requestLog);

    this.calculatePerf();
  }

  addRequestLogs(requestLogs: IRequestLog[]): void {
    this.requestLogs.push(...requestLogs);

    this.calculatePerf();
  }

  getTotalRequestsCount(): number {
    return this.logRecordsScanned;
  }

  getFailureRate(): number {
    return this.failedRequestsCount / this.logRecordsScanned;
  }

  getSuccessRate(): number {
    return (this.logRecordsScanned - this.failedRequestsCount) / this.logRecordsScanned;
  }

  getAvgResponseTime(): number {
    return this.logRecordsScanned > 0 ? this.totalResponseTime / this.logRecordsScanned : 0;
  }

  getMaxResponseTime(): number {
    return this.maxResponseTime;
  }

  getRequestsPerMin(): number {
    return this.requestsPerMinute;
  }

  getFailuresPerMin(): number {
    return this.failuresPerMinute;
  }
}

export default PerfAnalysis;

class RequestError extends Error {
  private errorResponse: unknown;

  constructor(message: string, errorResponse?: unknown) {
    super(message);
    this.errorResponse = errorResponse;
    this.name = 'RequestError';
  }

  getApiResponse() {
    return this.errorResponse;
  }
}

export default RequestError;

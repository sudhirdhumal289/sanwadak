import { parseTemplate } from 'url-template';
import { IEndpointConfig } from '../types/api.types';
import { IUrlPathParams } from '../types/request.types';

class Endpoint {
  private config: IEndpointConfig;

  constructor(config: IEndpointConfig) {
    this.config = config;

    this.validateConfiguration();
  }

  private validateConfiguration() {
    // TODO - add validation
  }

  getEndpointConfig(): IEndpointConfig {
    return this.config;
  }

  getParsedPath(urlPathParams?: IUrlPathParams): string {
    if (urlPathParams) {
      const urlTemplate = parseTemplate(this.config.path);

      return urlTemplate.expand(urlPathParams);
    }
    
    return this.config.path;
  }
}

export default Endpoint;

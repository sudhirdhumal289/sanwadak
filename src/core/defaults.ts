import { IStrategyConfig } from '../types/api.types';

export const defaultStrategyConfig: IStrategyConfig = {
  timeout: 1000 * 60,
  retryPolicy: {
    delayTimeInMilliSeconds: 1000,
    retryCount: 3, // Retry attempts are only for 500+ HTTP status code
  },
};

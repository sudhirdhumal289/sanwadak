import axios from 'axios';
import { IRetryPolicy } from '../types/api.types';
import { defaultStrategyConfig } from './defaults';
import { IRequestLog, IRetryResult } from '../types/request.types';

function wait(delay: number): Promise<void> {
  let waitPeriod = delay;
  if (!delay || delay <= 0) {
    waitPeriod = (defaultStrategyConfig.retryPolicy as IRetryPolicy).delayTimeInMilliSeconds;
  }

  return new Promise((resolve) => setTimeout(resolve, waitPeriod));
}

/* eslint-disable @typescript-eslint/no-explicit-any */
type FunctionToExecute = (...args: any[]) => Promise<unknown>;

export interface IRetryExecutionConfig {
  functionToExecute: FunctionToExecute;
  args: Parameters<FunctionToExecute>;
  retryPolicy: IRetryPolicy;
}

const retry = async (retryConfig: IRetryExecutionConfig): Promise<IRetryResult> => {
  const retryCount = retryConfig.retryPolicy.retryCount > 0 ? retryConfig.retryPolicy.retryCount : 0;

  const requestLogs: IRequestLog[] = [];

  for (let i = 0; i < retryCount; i++) {
    const startTime = new Date();

    try {
      /* eslint-disable @typescript-eslint/no-unsafe-argument */
      const axiosResponse = await retryConfig.functionToExecute(...retryConfig.args);

      requestLogs.push({
        startTime,
        endTime: new Date(),
        isFailed: false,
      });

      return { axiosResponse, requestLogs };
    } catch (error) {
      requestLogs.push({
        startTime,
        endTime: new Date(),
        isFailed: true,
      });

      if (axios.isAxiosError(error) && error.response && error.response.status >= 500) {
        await wait(retryConfig.retryPolicy.delayTimeInMilliSeconds);

        if (i + 1 < retryCount) continue;
      }

      throw error;
    }
  }

  // Technically this line wont be executed but language can't infer this information.
  // So we are returning generic informative error message
  throw new Error('Failed to retry the function execution.');
};

export default retry;

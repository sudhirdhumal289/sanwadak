import { URLSearchParams } from 'url';
import { AxiosRequestConfig, AxiosRequestHeaders } from 'axios';

export interface IUrlPathParams {
  /* eslint-disable @typescript-eslint/no-explicit-any */
  readonly [param: string]: any;
}

export interface IRequestConfig<D> {
  endpointId: string;
  retryEnabled?: boolean;
  pathParams?: IUrlPathParams;
  searchParams?: URLSearchParams;
  payload?: D
  headers?: AxiosRequestHeaders;
  axiosRequestConfig?: AxiosRequestConfig<D>;
}

export type INoDataRequestConfig = Omit<IRequestConfig<null>, 'payload'>;

export type IRequestLog = {
  startTime: Date;
  endTime: Date;
  isFailed: boolean;
};

export interface IRetryResult {
  axiosResponse: unknown,
  requestLogs: IRequestLog[],
}

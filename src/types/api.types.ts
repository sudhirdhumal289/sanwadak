type AxiosRequestHeaders = {
  [header: string]: string;
}

export enum HttpVerb {
  GET = 'GET',
  HEAD = 'HEAD',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  OPTIONS = 'OPTIONS',
  PATCH = 'PATCH'
}

export interface IRetryPolicy {
  delayTimeInMilliSeconds: number;
  retryCount: number;
}

export interface IStrategyConfig {
  timeout: number;
  retryPolicy?: IRetryPolicy;
}

export interface IEndpointConfig {
  id: string;
  path: string;
  verb: HttpVerb;

  displayName?: string;
}

export interface IClusterConfig {
  id: string;
  baseUrls: string[];
  endpointConfigs: IEndpointConfig[];
  minServerCountForPerfCalc?: number;

  displayName?: string;
  strategyConfig?: IStrategyConfig;
  defaultHeaders?: AxiosRequestHeaders;
}

export interface IServerConfig {
  id: string;
  baseUrl: string;
  endpointConfigs: IEndpointConfig[];
  clusterId: string;
  strategyConfig?: IStrategyConfig;
  defaultHeaders?: AxiosRequestHeaders;
}

export interface IApiConfig {
  clusters: IClusterConfig[];
  strategyConfig?: IStrategyConfig;
  debugMode?: boolean;
}
